package com.Mortgage.broker.util;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;

public class BrowserOperations {
	
	WebDriver driver;

	public WebDriver getDriver()
	{
		return driver;
		
	}
	
	@Parameters({"browser", "baseURL"})
	@BeforeTest
	public void openBrowser(String browser, String baseURL)
	{
		
		if(browser.equalsIgnoreCase("FF"))
		{
			System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir")+"\\src\\test\\resources\\Lib\\geckodriver.exe");
			driver = new FirefoxDriver();
		}
		
		else if(browser.equalsIgnoreCase("CH"))
		{
			System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"\\src\\test\\resources\\Lib\\chromedriver.exe");
			driver = new ChromeDriver();
		}
		
		else
		{
			System.setProperty("webdriver.ie.driver", System.getProperty("user.dir")+"\\src\\test\\resources\\Lib\\IEDriverServer.exe");
			driver = new InternetExplorerDriver();
		}
		
		driver.get(baseURL);
	}
	
	

	@AfterTest
	public void closeBrowser()
	{
		driver.quit();
	}
	
	

}